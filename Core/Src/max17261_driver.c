/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "max17261_driver.h"
#include "main.h"
#include "cmsis_os.h"

extern I2C_HandleTypeDef hi2c2;

/**
 * @brief I2C Read function
 * Reads 16bit data from device
 * @param reg Register to read from
 * @param value Pointer to write value
 * @return 0 on success error code otherwise
 */
uint8_t
max17261_read_word(struct max17261_conf *conf, uint8_t reg, uint16_t *value)
{
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t buf[2];

	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)MAX17261_ADDRESS, 1,
	                               1000);
	if (status != HAL_OK) {
		return status;
	}

	status = HAL_I2C_Mem_Read(&hi2c2, (uint8_t)MAX17261_ADDRESS,
	                          (uint8_t) reg, I2C_MEMADD_SIZE_8BIT, buf, 2, 1000);

	if (status != HAL_OK) {
		return status;
	}
	*value = (uint16_t)((buf[1] << 8) | buf[0]);
	return status;

}

/**
 * @brief I2C Write function
 * Writes 16bit data to device
 * @param reg Register to write to
 * @param value Value to write
 * @return 0 on success error code otherwise
 */
uint8_t
max17261_write_word(struct max17261_conf *conf, uint8_t reg, uint16_t value)
{
	HAL_StatusTypeDef status = HAL_OK;
	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)MAX17261_ADDRESS, 1,
	                               1000);
	if (status
	    != HAL_OK) {
		return status;
	}

	uint8_t array[2];
	array[0] = value & 0xff;
	array[1] = (value >> 8);

	status = HAL_I2C_Mem_Write(&hi2c2, (uint8_t)MAX17261_ADDRESS,
	                           (uint8_t) reg, I2C_MEMADD_SIZE_8BIT, array,
	                           2, 1000);

	if (status != HAL_OK) {
		return status;
	}
	return status;
}

uint8_t
max17261_delay_ms(struct max17261_conf *conf, uint32_t period)
{
	return osDelay(period);
}
