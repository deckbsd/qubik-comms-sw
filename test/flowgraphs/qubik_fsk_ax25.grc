options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: Qubik FSK/MSK AX.25 decoder
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: qubik_fsk_ax25
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: qubik_fsk_ax25
    window_size: 2*1080,1080
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 4.0]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1472, 20.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: max(4,satnogs.find_decimation(baudrate, 2, audio_samp_rate))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1248, 12.0]
    rotation: 0
    state: true
- name: variable_ax25_decoder_0
  id: variable_ax25_decoder
  parameters:
    addr: '''GND'''
    comment: ''
    crc_check: 'True'
    descrambling: 'True'
    frame_len: '1024'
    promisc: 'True'
    ssid: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1216, 788.0]
    rotation: 0
    state: enabled
- name: variable_ax25_decoder_0_0
  id: variable_ax25_decoder
  parameters:
    addr: '''GND'''
    comment: ''
    crc_check: 'True'
    descrambling: 'False'
    frame_len: '1024'
    promisc: 'True'
    ssid: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1392, 788.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.2'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1280, 524.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 524.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 12.0]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '9600.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1352, 12.0]
    rotation: 0
    state: enabled
- name: blocks_delay_0
  id: blocks_delay
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    delay: 1024//2
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 452.0]
    rotation: 0
    state: enabled
- name: blocks_moving_average_xx_0
  id: blocks_moving_average_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    max_iter: '4096'
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: 1.0/1024.0
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 500.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 456.0]
    rotation: 0
    state: enabled
- name: blocks_vco_c_0
  id: blocks_vco_c
  parameters:
    affinity: ''
    alias: ''
    amplitude: '1.0'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    sensitivity: -baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 508.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 12.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 516.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/.satnogs/data/data
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 860.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 12.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1480, 680.0]
    rotation: 180
    state: enabled
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: 0.5/8.0
    gain_omega: 2 * math.pi / 100
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: '2'
    omega_relative_limit: '0.01'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1608, 644.0]
    rotation: 180
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 860.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 780.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: test.wav
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 780.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1048, 12.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 164.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/iq.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 780.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [888, 12.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: 0.75 * baudrate
    decim: decimation // 2
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: "Perform a relaxed filter to increase \nthe performance of the auto frequency\n\
      correction"
    cutoff_freq: baudrate*1.25
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 2.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_1
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: baudrate * 0.60
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: 2 * baudrate
    type: fir_filter_fff
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 476.0]
    rotation: 0
    state: bypassed
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate_rx
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'True'
    fc: '0'
    fftsize: '4096'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 296.0]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: baudrate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1632, 788.0]
    rotation: 0
    state: true
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 868.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 12.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 12.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: baudrate*decimation
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 132.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ax25_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1220, 638]
    rotation: 180
    state: enabled
- name: satnogs_frame_decoder_0_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ax25_decoder_0_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1216, 712.0]
    rotation: 180
    state: enabled
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 508.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 684.0]
    rotation: 180
    state: true
- name: satnogs_multi_format_msg_sink_0
  id: satnogs_multi_format_msg_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filepath: ''
    format: '0'
    outstream: 'True'
    timestamp: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [816, 724.0]
    rotation: 180
    state: true
- name: satnogs_tcp_rigctl_msg_source_0
  id: satnogs_tcp_rigctl_msg_source
  parameters:
    addr: '"127.0.0.1"'
    affinity: ''
    alias: ''
    comment: ''
    interval: int(1000.0/doppler_correction_per_sec) + 1
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: 'False'
    mtu: '1500'
    port: rigctl_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 244.0]
    rotation: 0
    state: disabled
- name: satnogs_udp_msg_sink_0_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: udp_IP
    affinity: ''
    alias: ''
    comment: ''
    mtu: '1500'
    port: udp_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [816, 636.0]
    rotation: 180
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: driver=invalid
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 12.0]
    rotation: 0
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'False'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 108.0]
    rotation: 0
    state: true
- name: udp_IP
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: 127.0.0.1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 780.0]
    rotation: 0
    state: enabled
- name: udp_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16887'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 868.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [960, 164.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 452.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/waterfall.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 780.0]
    rotation: 0
    state: enabled

connections:
- [analog_quadrature_demod_cf_0_0, '0', low_pass_filter_1, '0']
- [analog_quadrature_demod_cf_0_0_0_0, '0', blocks_moving_average_xx_0, '0']
- [blocks_delay_0, '0', blocks_multiply_xx_0, '0']
- [blocks_moving_average_xx_0, '0', blocks_vco_c_0, '0']
- [blocks_multiply_xx_0, '0', low_pass_filter_0, '0']
- [blocks_vco_c_0, '0', blocks_multiply_xx_0, '1']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0_0_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', qtgui_time_sink_x_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_0_0, '0', analog_quadrature_demod_cf_0_0_0_0, '0']
- [low_pass_filter_1, '0', dc_blocker_xx_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_decoder_0_0_0, out, satnogs_json_converter_0, in]
- [satnogs_json_converter_0, out, satnogs_multi_format_msg_sink_0, in]
- [satnogs_json_converter_0, out, satnogs_udp_msg_sink_0_0, in]
- [satnogs_tcp_rigctl_msg_source_0, freq, satnogs_doppler_compensation_0, doppler]
- [soapy_source_0, '0', qtgui_freq_sink_x_0, '0']
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', blocks_delay_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']

metadata:
  file_format: 1
